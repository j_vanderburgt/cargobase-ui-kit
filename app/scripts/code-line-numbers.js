(function($) {

  var CodeLineNumers = function (element) {
    this.$element = $(element);
    this.$code    = this.$element.find('code:first');

    this.setNumbers();
  }

  CodeLineNumers.prototype.setNumbers = function () {
    var match = this.$code.text().match(/\n(?!$)/g);
    var linesNum = match ? match.length + 1 : 1;
    var lineNumbersWrapper;
    var lines = new Array(linesNum + 1);

    if (!this.$element.hasClass('code-line-numbers')) {
      this.$element.addClass('code-line-numbers');
    }

    lines = lines.join('<span></span>');

    lineNumbersWrapper = $('<span />');
    lineNumbersWrapper.addClass('code-line-numbers-rows');
    lineNumbersWrapper.html(lines);

    this.$element.append(lineNumbersWrapper);
  }

  function Plugin() {
    return this.each(function () {
      var $this     = $(this);
      var $children = $this.children();
      var data      = $this.data('cb.code-line-numbers');

      if ($this[0].tagName !== 'PRE' || $children[0].tagName !== 'CODE' || $children.length !== 1) {
        return;
      }

      if (!data) {
        $this.data('cb.code-line-numbers', (data = new CodeLineNumers(this)));
      }
    });
  }

  $.fn.codeLineNumbers = Plugin;
  $.fn.codeLineNumbers.Constructor = CodeLineNumers;

  $(window).on('load', function () {
    $('[data-toggle="code-line-numbers"]').each(function () {
      Plugin.call($(this));;
    });
  });

})(jQuery);