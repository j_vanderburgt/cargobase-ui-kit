(function ($) {

  var FormSelectMenu = function (element) {
    this.$element = null;
    this.$parent = null;
    this.$widget = null;

    this.init(element);
  };

  FormSelectMenu.OPENED_STATE_CLASS = 'ui-selectmenu-opened';

  FormSelectMenu.prototype.init = function (element) {
    this.$element = $(element);
    this.$parent = this.$element.closest('div');
    this.$widget = this.$element.selectmenu({
      appendTo: this.$parent,
      open: this.openHandler.bind(this),
      close: this.closeHandler.bind(this),
      width: '100%'
    });
  };

  FormSelectMenu.prototype.openHandler = function () {
    this.$element.addClass(FormSelectMenu.OPENED_STATE_CLASS);
  };

  FormSelectMenu.prototype.closeHandler = function () {
    this.$element.removeClass(FormSelectMenu.OPENED_STATE_CLASS);
  };

  function Plugin() {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('cb.form-selectmenu');

      if (!data) {
        $this.data('cb.form-selectmenu', (data = new FormSelectMenu(this)))
      }
    });
  };

  $.fn.formAutocomplete = Plugin;

  $(window).on('load', function () {
    $('[data-toggle="form-selectmenu"]').each(function () {
      Plugin.call($(this));
    });
  });

})(jQuery);