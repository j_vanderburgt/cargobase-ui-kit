(function ($) {

  $.widget('cb.autocomplete', $.ui.autocomplete, {
    _renderItem: function(ul, item) {
        var re = new RegExp('(' + this.term + ')', 'gi');
        var template = '<span class="' + FormAutocomplete.HIGHLIGHT_CLASS + '">$1</span>';
        var label = item.label.replace(re, template);
        var desc = item.desc ? item.desc.replace(re, template) : null;
        var $li = $('<li/>').appendTo(ul);

        if (desc) {
          $li.append('<strong>' + label + '</strong>' + desc);
        } else {
          $li.append(label);
        }

        return $li;
    }
  });

  var FormAutocomplete = function (element) {
    this.$element = null;
    this.$parent = null;
    this.$widget = null;

    this.init(element);
  };

  FormAutocomplete.OPENED_STATE_CLASS = 'ui-autocomplete-opened';
  FormAutocomplete.HIGHLIGHT_CLASS    = 'ui-state-highlight';

  FormAutocomplete.prototype.init = function (element) {
    var data;

    this.$element = $(element);
    this.$parent = this.$element.closest('div');

    data = this.$element.data();

    this.$widget = this.$element.autocomplete({
      appendTo: this.$parent,
      source: data.options ? data.options.split(',') : data.src,
      open: this.openHandler.bind(this),
      close: this.closeHandler.bind(this)
    });
  };

  FormAutocomplete.prototype.openHandler = function () {
    this.$element.addClass(FormAutocomplete.OPENED_STATE_CLASS);
  };

  FormAutocomplete.prototype.closeHandler = function () {
    this.$element.removeClass(FormAutocomplete.OPENED_STATE_CLASS);
  };

  function Plugin() {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('cb.form-autocomplete');

      if (!$this.data('options') && !$this.data('src')) {
        return;
      }

      if (!data) {
        $this.data('cb.form-autocomplete', (data = new FormAutocomplete(this)))
      }
    });
  };

  $.fn.formAutocomplete = Plugin;

  $(window).on('load', function () {
    $('[data-toggle="form-autocomplete"]').each(function () {
      Plugin.call($(this));
    });
  });

})(jQuery);